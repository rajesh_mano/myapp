/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.elakkiya.myapp.web.rest.dto;
